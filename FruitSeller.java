/*   Class Item which demonstrates the use of static members in the Item class for counting the number of fruits added in the program.
	SENERIO: I have taken the senerio of a Fruit Seller who wants to enter the fruits he has and print them in dozens....
					-Krima 12/2/15		*/


import java.util.Scanner;

class Item							//Class which stores the data of the individual fruits entered
{
	static int item_no;					//keeps track of the number of items
	int number;						
	String fruit_name;
	int dozens;						//other details that are stored
	Item next;						//next node	

	Item()
	{
		item_no++;
		next=null;
	}	
		
	void set_data()						// function when called lets you input data
	{
		Scanner s=new Scanner(System.in);
		System.out.println("Number of items so far:" + item_no);
		System.out.println("Enter the name of fruit you would like to enter:");
		fruit_name=s.next();
		System.out.println("Enter the number of " + fruit_name);
		number=s.nextInt();
		dozens=number/12;	
	}

	void get_data()						// function that lets you extract data
	{
		System.out.println("Total item number "+ item_no);
		System.out.println("Number of items "+ number);
		System.out.println("Fruit name "+ fruit_name);
		System.out.println("Number of dozens= " + dozens);
	}
	
}

class ItemsLL								//List containing main function
{
	public static void main(String arg[])
	{
		int n,i=0;						//initial number of fruits-n and a initiator-i
		char c='y';						//choice variable-c (later on)
		Item head,last,cur;						// node of first and next fruits
		head=null;
		last=null;						//initializing to null

		Scanner s=new Scanner(System.in);
		System.out.println("Enter the number of fruits you would like to enter:");
		n=s.nextInt();						//number of fruits to be entered initially

		if (i==0)
		{
			
			head=new Item();			
			head.set_data();	
			last=head;					// for first input
		}		
		for(i=1;i<n;i++)
		{
			last.next=new Item();	
			last=last.next;
			if (i==1)
				last=head.next;
			last.set_data();				//for all other inputs upto n	
		}
		
		while(c=='Y'||c=='y')
		{
			System.out.println("Would you like to enter more data? y/n: ");
			c=s.next().charAt(0);				// for entering more inputs than n
		
			if  (c=='y' || c== 'Y')
			{	
				last.next=new Item();	
				last=last.next;
				last.set_data();
			}
		}

		for (cur=head; cur != null ; cur=cur.next)		//prints all data
		{
		 	cur.get_data();		
		}
			
	}
}