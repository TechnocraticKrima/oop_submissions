/*  Design a class in C++ called “Polynomial” that would store two dynamic arrays. The first array
would represent the co-efficient of the terms and the second array would represent the degree (or
exponent) of the terms. Create appropriate constructor(s) and destructor for the class.
***Overload the “+” operator to add two polynomials and store the result in the third polynomial.
***Overload the “*”operator to multiply a polynomial with a scalar value and store in another polynomial.
***Overload the unary “–” that would assign negative of a polynomial to another polynomial.
***Overload the “<<” and “>>” operators to read and display a polynomial.
                                                                        -Krima 23-02-15 */

#include <iostream>

using namespace std;

class Polynomial                                                        //The class containing all elements
{
    friend std::ostream& operator <<(std::ostream &,Polynomial&);       //friend classes
    friend std::istream&  operator >>(std::istream &,Polynomial&);
private:                                                                //two arrays with max power initialized
    int power=0;
    int *coefficient;
    int *degree;
public:
    Polynomial(){}                                                      //empty constructor

    Polynomial(int n)                                                   //initializing constructor
    {
        degree=new int[n];
        coefficient=new int[n];
        for(power=0;power<n;power++)
        {
            degree[power]=power;
            cout<<"Enter coefficient of variable to the power"<<power;
            cin>>coefficient[power];
        }
    }

    Polynomial(int n, int dummy)                                        //only degree initializing constructor
    {
        degree=new int[n];
        coefficient=new int[n];
        for(power=0;power<n;power++)
        {
            degree[power]=power;
        }
    }

    Polynomial(Polynomial x, int n)                                     //copy constructor which copies the elements of one array and initializing the rest to 0
    {
        degree=new int[n];
        coefficient=new int[n];
        for(power=0;power<x.power;power++)
        {
            degree[power]=power;
            coefficient[power]=x.coefficient[power];
        }
        for(power=x.power;power<n;power++)
        {
            degree[power]=power;
            coefficient[power]=0;
        }
    }

    ~Polynomial()                                                       //Destructor
    {
        delete degree;
        delete coefficient;
    }

    Polynomial operator+(Polynomial);                                   //Declaring the operator overloaders
    Polynomial operator*(int);
    Polynomial operator-();

};


std::ostream& operator <<(std::ostream &out,Polynomial &p1)             //cout overloading friend function
    {
        int i;
        out<<" The coefficient and power of the polynomial are: \n Coefficient \t Power";
        out<<"\n";
        for(i=0;i<p1.power;i++)
        {
            out<<p1.coefficient[i];
            out<<"\t\t\t";
            out<<p1.degree[i];
            out<<"\n";
        }
        return out;

    }
std::istream& operator >>(std::istream &in,Polynomial &p1)              //cin overloading friend function
    {
        int i;
        for(i=0;i<p1.power;i++)
        {
            p1.degree[i]=i;
            cout<<"Enter coefficient of variable to the power"<<i;
            in>>p1.coefficient[i];
        }
        return in;
    }


Polynomial Polynomial::operator+(Polynomial p2)                         //+ overloading mamber function
{
    int no,i;
    if(power>p2.power)                                                  //if max power of this>p2, add takes all values of p2 and initializes the rest to 0
     {
         no=power;
         Polynomial add(p2,no);
        for(i=0;i<no;i++)
        {
            add.coefficient[i]+=coefficient[i];
        }
        return add;
     }
    else                                                                //if max power of this<=p2, add takes all values of this and initializes the rest to 0
    {
         no=p2.power;
         Polynomial add(*this,no);
        for(i=0;i<no;i++)
        {
            add.coefficient[i]+=p2.coefficient[i];
        }
        return add;
     }


}

Polynomial Polynomial::operator*(int multiply)                          //* overloading mamber function
{
    int i;
    Polynomial mult(power,power);
    for(i=0;i<power;i++)
    {
        mult.coefficient[i]=coefficient[i]*multiply;
    }
    return mult;

}

Polynomial Polynomial::operator-()                                      //unary overloading mamber function
{
    int i;
    Polynomial negative(power,power);
    for(i=0;i<power;i++)
    {
        negative.coefficient[i]=-coefficient[i];
    }
    return negative;

}

int main()                                                              //mainfunction where all functions are performed
{
    int k,n,n1,n2;

    cout<<"Enter number of elements:";
    cin>>n1;
    Polynomial p1(n1);                                                  //declaring p1 of max power n1

    cout<<"Enter number of elements:";
    cin>>n2;
    Polynomial p2(n2);                                                  //declaring p2 of max power n2

    if(n1>n2)
        n=n1;
    else
        n=n2;

    Polynomial result(n,n);                                             //declaring result of maximum power n
    result = p1 + p2;                                                   //+ overloading
    cout<<result;                                                       //<< overloading

    result = -p1;                                                       //- overloading
    cout<<result;

    cout<<"Enter number you would like to multiply:";
    cin>>k;
    result = p1*k;                                                      //* overloading
    cout<<result;

    cin>>result;                                                        //>> overloading
    cout<<result;
    return 0;
}

