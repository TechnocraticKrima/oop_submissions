STEPS TO INSTALL JAVA ON LINUX:

#1 Install a Java Runtime Environment onto your computer:
	1.open a terminal window. When it opens, copy and paste the command below and hit enter. Input your user password when prompts and it will add the 	PPA repository into your system.

		sudo add-apt-repository ppa:webupd8team/java

	2.After that, update package lists via:

		sudo apt-get update

	3.To install Oracle Java 8, run:
	
		sudo apt-get install oracle-java8-installer

	Change the number 8 to 6 (or 7) in the code to install Java 6 (or 7).

	While installation, you’ll be asked to agree the license and then the installer start downloading Java file from oracle website and install it on 		your system.

	4.To set the default Java, run:

		sudo apt-get install oracle-java8-set-default

	Also change number 8 to the Java version you want.

	5.Finally check whether everyting is OK:

		java -version

	It will output something like below:

	            java version “1.7.0_60″
		    Java(TM) SE Runtime Environment (build 1.7.0_60-b19)
		    Java HotSpot(TM) 64-Bit Server VM (build 24.60-b09, mixed mode)



#2 Install a java sdk file onto your computer

	The tar.gz provided by Oracle don't have an actual installation process. You just extract those files to a location you want and add them to your 	path. So the process is the following:

	    Download a .tar.gz from Oracle (here I will be using jdk-8u20-linux-x64.tar.gz);
	    Extract it to somewhere;
	
	    Move the extracted folder to /usr/lib/jvm. This is not required but it is the place where Java runtime software is installed
	
	    sudo mv /path/to/jdk1.8.0_20 /usr/lib/jvm/oracle_jdk8
	
	    Create a file /etc/profile.d/oraclejdk.sh with the following content (adapt the paths to reflect the path where you stored your JDK):
	
	    export J2SDKDIR=/usr/lib/jvm/oracle_jdk8
	    export J2REDIR=/usr/lib/jvm/oracle_jdk8/jre
	    export PATH=$PATH:/usr/lib/jvm/oracle_jdk8/bin:/usr/lib/jvm/oracle_jdk8/db/bin:/usr/lib/jvm/oracle_jdk8/jre/bin
	    export JAVA_HOME=/usr/lib/jvm/oracle_jdk8
	    export DERBY_HOME=/usr/lib/jvm/oracle_jdk8/db
	
	    Make it executable:
	
	    sudo chmod +x /etc/profile.d/oraclejdk.sh
	
		Done! Those paths will only be recognized after you logout or restart, so if you want to use them right away run source 					/etc/profile.d/oraclejdk.sh.


NOTE: Make sure you have transferred your sdk file into the proper default java runtime environment or else an error will appear everytime you execute javac.

