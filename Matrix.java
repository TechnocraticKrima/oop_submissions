/*	Design a class called “Matrix” in Java to store various attributes required for a dynamic matrix.
Create appropriate constructors (including a copy constructor) for the class. Create appropriate
member functions for performing the following operations:
(a)Assign values to a matrix
(b)Extract values from a matrix
(c)Find transpose of a matrix
(d)Add two matrices after checking validity of the operation
(e)Multiply two matrices after checking validity of the operation
(f)Multiply a matrix with a scalar value
											-Krima 19/02/15	*/


import java.util.Scanner;
class MatrixValues                                                      //class storing all neccessary values for matrices
{

	int matrix[][],i,j;                                                 //declaration of variables
	int row,column;
	Scanner s=new Scanner(System.in);

	MatrixValues()                                                      //constructor which when called inputs info into matrix
	{
		System.out.println("Enter number of rows in matrix");
		row=s.nextInt();
		System.out.println("Enter number of columns in matrix");
		column=s.nextInt();

		matrix=new int[row][column];
		for (i=0;i<row;i++)
		{
			for (j=0;j<column;j++)
			{
				System.out.println("Enter element" + i + "  " + j);
				matrix[i][j]=s.nextInt();
			}
		}
	}

	void viewValues()                                                   //to view values of given matrix
	{
		for (i=0;i<row;i++)
		{
			for (j=0;j<column;j++)
			{
				System.out.print(matrix[i][j]+"\t");
			}
			System.out.print("\n");
		}

	}

	void ScalarMultiply(int k)                                          //multiply matrix with scalar
	{
		for (i=0;i<row;i++)
		{
			for (j=0;j<column;j++)
			{
				matrix[i][j]*=k;
			}
		}
	}
}

class Transpose                                                         //class storing all neccessary info for transpose of all matrices so it can be used in future
{

	static int transpose[][],i,j;
	int row,column;

	Transpose(MatrixValues m)                                           //function for finding transpose
	{
		transpose=new int[m.column][m.row];
		row=m.column;
		column=m.row;
		for (i=0;i<row;i++)
		{
			for (j=0;j<column;j++)
			{
				transpose[i][j]=m.matrix[j][i];
			}
		}
	}

	void viewValues()                                                   //veiw values of a transpose
	{
		for (i=0;i<row;i++)
		{
			for (j=0;j<column;j++)
			{
				System.out.print(transpose[i][j]+"\t");
			}
			System.out.print("\n");
		}
	}
}

class Matrix                                                            //class containing main function&menu
{
	public static void main(String args[])
	{
		int choice=0,n,mat_no,mat_no1;;
		Scanner s=new Scanner(System.in);

		System.out.println("How many matrices would you like to enter?");
		n=s.nextInt();                                                  //total number of matrices that can be entered& their declaration
		MatrixValues matrix[]=new MatrixValues[n];
		Transpose transpose[]=new Transpose[n];

		while(choice!=7)
		{
			System.out.println("\t\t\t MENU \t\t\t\n 1-Assign values to a matrix \n 2-View a matrix \n 3-Find the transpose of a matrix \n 4-add two matrices \n 5-multiply two matrices \n 6-multiply a matrice with a scalar \n 7-EXIT");
			choice=s.nextInt();

			switch(choice)                                              //choice menu
			{
			case 1:                                                     //assign values to a matrix
				System.out.println("Enter the matrix you would like to assign value of:(starting from 0 to"+(n-1)+")");
				mat_no=s.nextInt();
				matrix[mat_no]=new MatrixValues();
				break;

			case 2:                                                     //view values of a matrix
				System.out.println("Enter the matrix you would like to view:(starting from 0 to"+(n-1)+")");
				mat_no=s.nextInt();
				matrix[mat_no].viewValues();
				break;

			case 3:                                                     //transpose of a matrix
				System.out.println("Enter the matrix you would like to find the transpose of:(starting from 0 to"+(n-1)+")");
				mat_no=s.nextInt();
				transpose[mat_no]=new Transpose(matrix[mat_no]);
				transpose[mat_no].viewValues();
				break;

			case 4:                                                     //add two matrices
				System.out.println("Enter the first matrix you would like to add:(starting from 0 to"+(n-1)+")");
				mat_no=s.nextInt();
				System.out.println("Enter the second matrix you would like to add:(starting from 0 to"+(n-1)+")");
				mat_no1=s.nextInt();
				if (matrix[mat_no].row==matrix[mat_no1].row && matrix[mat_no].column==matrix[mat_no1].column)
				{
					add(matrix[mat_no], matrix[mat_no1]);
				}
				else
					System.out.println("The dimensions of the matrices are different hence they cannot be added");
				break;

			case 5:                                                     //multiply two matrices
				System.out.println("Enter the first matrix you would like to multiply:(starting from 0 to"+(n-1)+")");
				mat_no=s.nextInt();
				System.out.println("Enter the second matrix you would like to multiply:(starting from 0 to"+(n-1)+")");
				mat_no1=s.nextInt();
				if (matrix[mat_no1].row == matrix[mat_no].column)
				{
					multiply(matrix[mat_no], matrix[mat_no1]);
				}
				else
					System.out.println("The dimensions of the matrices are different hence they cannot be multiplied");
				break;

			case 6:                                                     //scalar multiplication of a matrix
				System.out.println("Enter the matrix you would like to multiply:(starting from 0 to"+(n-1)+")");
				mat_no=s.nextInt();
				System.out.println("Enter the number you would like the matrix to be multiplied with");
				mat_no1=s.nextInt();
				matrix[mat_no].ScalarMultiply(mat_no1);
				matrix[mat_no].viewValues();
				break;
			case 7:                                                     //EXIT
				break;

			default:                                                    //otherwise
				System.out.println("Please enter a valid choice");
				break;

			}
		}
	}


	static void add(MatrixValues m1, MatrixValues m2)
	{
		int i,j;
		int add[][]=new int[m1.row][m1.column];
		for (i=0;i<m1.row;i++)
		{
			for (j=0;j<m1.column;j++)
			{
				add[i][j]=m1.matrix[i][j]+m2.matrix[i][j];
			}
		}

		for (i=0;i<m1.row;i++)
		{
			for (j=0;j<m1.column;j++)
			{
				System.out.print(add[i][j]+"\t");
			}
			System.out.print("\n");
		}
	}

	static void multiply(MatrixValues m1, MatrixValues m2)
	{
		int i,j,k;
		int mult[][]=new int[m1.row][m2.column];
		for (i=0;i<m1.row;i++)
		{
			for (j=0;j<m2.column;j++)
			{
				for (k=0;k<m2.row;k++)
				{
					mult[i][j]+=m1.matrix[i][k]*m2.matrix[k][j];
				}
			}
		}

		for (i=0;i<m1.row;i++)
		{
			for (j=0;j<m2.column;j++)
			{
				System.out.print(mult[i][j]+"\t");
			}
			System.out.print("\n");
		}
	}
}
