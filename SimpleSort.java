/* A program to do simple sorting  
			-Krima 11/02/15	*/

import java.util.Scanner;

class SimpleSort
{
	public static void main(String args[])
	{
		int i,j,num;
		Scanner s=new Scanner(System.in);
		System.out.println("Enter the number of numbers you would like to sort:");
		num=s.nextInt();
		
		int arr[]=new int[num];
		for(i=0;i<num;i++)	
		{
			System.out.println("Enter number "+ i + ": ");
			arr[i]=s.nextInt();
	
		}
		
		for (i=0;i<num;i++)
		{
			for (j=0;j<i;j++)
			{
				if (arr[j]>arr[i])
				{	
					int temp;
					temp=arr[i];
					arr[i]=arr[j];
					arr[j]=temp;
				}	
		
			}
		}

		for(i=0;i<num;i++)	
		{
			System.out.println(" " + arr[i] + " ");
	
		}

	}

}
