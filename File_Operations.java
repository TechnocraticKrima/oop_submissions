/* Create a program in Java to perform the following operations on text files:
 Count the number of characters
 Count the number of words
 Count the number of lines
 Copy the contents of one file to another
 Rename a file
							-Krima 30/3/15	*/

import java.util.Scanner;
import java.io.*;

class Files								//Stores data of file along with associated operations
{
	String name;
	File f;
	FileWriter fw;
	
	Files(String s)							//Makes a file
	{
		try
		{
			f= new File(s+".dat");
			name=s;
			System.out.println("\n File Successfully made!");
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}

	Files(Files k)							//Generating copy of a file
	{
		f= new File(k.name+"_copy.dat");
		copy(k);
		System.out.println("\n File Successfully copied!");
		name=k.name;
	}
	
	Files(Files k, String s)					//Renaming a file
	{
		f= new File(s+".dat");
		copy(k);
		k.f.delete();
		System.out.println("\n File Successfully renamed!");
		name=s;
	}
	
	void Write()							//Writing to a file
	{
		try
		{
			char ch;
			fw= new FileWriter(f);
			Scanner s=new Scanner(System.in);

			while((ch=(char)System.in.read()) != '~')
			{	
				fw.write(ch);
			}
			fw.write("\n");
			fw.close();
		}
		catch(Exception e)
		{
			System.out.println(e);
		}

	}
		
	void copy(Files k)						//Copy contents of one file to another
	{
		try
		{
			int ch;
			fw= new FileWriter(f);
			FileReader fr=new FileReader(k.f);

			while((ch=fr.read()) != -1)
			{	
				fw.write((char)ch);
			}
			fw.write("\n");
			fw.close();
			fr.close();
		}
		catch(Exception e)
		{
			System.out.println(e);
		}

	}

	void Count()							//Counting characters, lines and words in a file
	{
		try
		{
			FileReader fr=new FileReader(f);
			int ch;
			int lines=0,charact=0, words=0;
			System.out.println("\n");
			
			while((ch=fr.read()) != -1)
			{
				if (((char)ch)=='\n')
				{
					lines++;
					words++;
				}
				else if (((char)ch) ==' ')
					words++;
				else 
					charact++;
		
			}
			fr.close();

			System.out.println("Lines in file: \t" + lines);
			System.out.println("Words in file: \t" + words);
			System.out.println("Characters in file: \t" + charact);	
		}
		catch(IOException e)
		{
			System.out.println(e);
		}	
	}

	String getName()
	{
		return name;
	}

}

class File_Operations							//Performing operations in a file
{
	public static void main(String args[])
	{
		int ch=0,number,i=0,j;
		String name;
		Scanner s=new Scanner(System.in);
		System.out.println("How many files would you like to make?");
		number=s.nextInt();

		Files f[]=new Files[number];

		while (ch!=5)
		{
			
			System.out.println("What would you like to do?");
			System.out.println("1-Add a new file");
			System.out.println("2-Copy a file into another");
			System.out.println("3-Rename a file");
			System.out.println("4-Count the characters, words and lines in a file");
			System.out.println("5-EXIT");
			ch=s.nextInt();

			switch(ch)
			{
			case 1:								//Adds a new file
				System.out.println("Enter name of the new file");
				name=s.next();
				f[i]=new Files(name);
				f[i].Write();
				i++;
				break;

			case 2:								//Copies a file
				System.out.println("Enter name of the file to be copied(with extention):");
				name=s.next();
				for(j=0; !name.equals(f[j].getName()+".dat") && j<i;j++){}
				f[i]=new Files(f[j]);
				i++;
				break;
			
			case 3:								//Renames a file
				System.out.println("Enter name of the file to be renamed(with extention):");
				name=s.next();
				for(j=0; !name.equals(f[j].getName()+".dat") && j<i;j++){}

				System.out.println("Enter new name of the file:");
				name=s.next();
				f[i]=new Files(f[j],name);
				i++;
				break;

			case 4:								//Counting in a file
				System.out.println("Enter name of the file to be counted(with extention)");
				name=s.next();
				for(j=0; !name.equals(f[j].getName()+".dat") && j<i;j++){}
				f[j].Count();
				break;

			case 5:								//EXIT
				break;

			default:
				System.out.println("Please enter a valid choice");
				break;
			}
		
		}

	}
}