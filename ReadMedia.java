/*	Design a class called “Media” in Java containing title, yearOfPublication, and price as data
members. Inherit the “Media” class into two classes called “Book” and “CD”. 
-The “Book” class shouldcontain author, and no_of_pages as data members, while 
-the “CD” class should contain sizeInMB”and playtime as data members.
Create appropriate constructors for all the three classes. Create appropriate member functions in order to read and display the values of a book and CD from the user. Implement the above mentioned classes using a menu driven program in a main function.
												-Krima 12/3/15	 */

import java.util.Scanner;
class Media
{
	String title;
	int yearOfPublication;
	Media()
	{
		yearOfPublication=0;
		title=null;
	}
	
	void setMedia(int y, String t)
	{
		
		yearOfPublication=y;
		title=t;	
		System.out.println("Name of Book:\t\t\t"+title);
		System.out.println("Year of Publication:\t\t\t"+yearOfPublication);
	}

	void getMedia()
	{
		System.out.println("Name of Book:\t\t\t"+title);
		System.out.println("Year of Publication:\t\t\t"+yearOfPublication);
	}
}

class Book extends Media
{
	String author;
	int no_of_pages;

	Book()
	{
		author=null;
		no_of_pages=0;

	}

	void setBook(int year, String title, String authoR, int pages)
	{
		setMedia(year,title);
		author=authoR;
		no_of_pages=pages;
		System.out.println("S");
	}
	
	void setAuthor(String authoR)
	{
		author=authoR;
	}
	
	void setPages(int pages)
	{
		no_of_pages=pages;
	}
	
	String getAuthor()
	{
		return author;
	}
	
	int getPages()
	{
		return no_of_pages;
	}
	
	void getBookDetails()
	{
		System.out.println("GET");
		getMedia();
		System.out.println("Author:\t\t\t"+ getAuthor());
		System.out.println("No. of pages:\t\t\t"+ getPages());
	}

}

class CD extends Media
{
	float sizeInMB;
	String playTime;

	CD()
	{

		sizeInMB=0;
		playTime=null;

	}

	void setCD(int year, String title, float mb, int seconds, int minutes)
	{
		setMedia(year,title);
		sizeInMB=mb;
		playTime= (minutes/60)+":"+(minutes%60)+":"+seconds;
	}
	
	void setMB(float mb)
	{
		sizeInMB=mb;
	}
	
	void setPlayTime(int seconds, int minutes)
	{
		playTime= (minutes/60)+":"+(minutes%60)+":"+seconds;
	}

	float getMB()
	{
		return sizeInMB;
	}
	
	String getPlayTime()
	{
		return playTime;
	}

	void getCDDetails()
	{
		getMedia();
		System.out.println("Size in MB:\t\t\t"+ sizeInMB);
		System.out.println("Play Time:\t\t\t"+playTime);
	}
}

class ReadMedia
{
	public static void main(String args[])
	{
		int c=0,choice;
		CD c1=new CD();
		Book b1=new Book();
		Scanner s=new Scanner(System.in);
		while(c!=3)
		{
			System.out.println("MENU \n 1-Go to book section \n 2-Go to CD section \n 3-exit");
			c=s.nextInt();
			switch(c)
			{
			case 1:
				choice=0;
				while(choice!=4)
				{
					int y,page;
					String title, author;
					System.out.println("\t\t\t BOOKS SECTION");
					System.out.println("\t\t\t*******************************");
					System.out.println("What would you like to do? \n 1- Add a book \n 2-edit details of a book");
					System.out.println(" 3-Get details of a book \n otherwise-EXIT");
					choice=s.nextInt();

					switch(choice)
					{
					
					case 1:
						System.out.println("Enter year of Publication, Title, Author and number of pages");
						y=s.nextInt();
						title=s.nextLine();
						title=s.nextLine();
						author=s.nextLine();
						page=s.nextInt();

						b1.setBook(y,title,author,page);
						break;

					case 2:
						b1.getBookDetails();
						System.out.println("Would you like to edit 1-the author 2-number of pages");
						y=s.nextInt();
						if(y==1)
						{
							System.out.println("Enter new author:");
							author= s.next();
							b1.setAuthor(author);
						}
						else if(y==2)
						{
							System.out.println("Enter new pages:");
							page=s.nextInt();
							b1.setPages(page);
						}
						break;

					case 3:
						b1.getBookDetails();
						break;
					
					default:
						break;
							
						
					}
				}
				break;
			case 2:
				choice=0;
				while(choice!=4)
				{
					int y, second,minutes;
					float mb;
					String title;
					System.out.println("\t\t\t CD SECTION");
					System.out.println("\t\t\t*******************************");
					System.out.println("What would you like to do? \n 1- Add a CD \n 2-edit details of a CD");
					System.out.println(" 3-Get details of a CD \n otherwise-EXIT");
					choice=s.nextInt();
					
					switch(choice)
					{
					
					case 1:
						System.out.println("Enter year of Publication, Title, MB size and length in minutes and seconds:");
						y=s.nextInt();
						title=s.next();
						mb=s.nextFloat();
						minutes=s.nextInt();
						second=s.nextInt();
						c1.setCD(y,title,mb,second,minutes);
						break;

					case 2:
						c1.getCDDetails();
						System.out.println("Would you like to edit 1-MB size 2-length of CD");
						y=s.nextInt();
						if(y==1)
						{
							System.out.println("Enter new MB size:");
							mb=s.nextFloat();
							c1.setMB(mb);
						}
						else if(y==2)
						{
							System.out.println("Enter new length in minutes and seconds:");
							minutes=s.nextInt();
							second=s.nextInt();
							c1.setPlayTime(second, minutes);
						}
						break;

					case 3:
						c1.getCDDetails();
						break;
					
					default:
						break;
							
						
					}
				}
				break;
			case 3:
				break;
			default:
				System.out.println("Enter a valid choice:");


			}
		}		

	}
}