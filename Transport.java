/*				Vehicles    ----------(containership)-------Engine
			___________|___________
		   Two Wheeler		 Four Wheeler
				      _________|________
				   Private Car	  Commercial Car
								-Krima 9/4/15	*/


import java.util.*;
import java.io.*;

class Engine									//Details of engine
{
	String typeOfFuel;	
	int noOfCylinders;

	Engine()
	{
		typeOfFuel="Petrol";
		noOfCylinders=2;
	}
	
	void setFuel(String fuel)
	{
		typeOfFuel=fuel;
	}
	void setCylinders(int cylin)
	{
		noOfCylinders=cylin;
	}
}

abstract class Vehicles							//Vehicle details
{
	Engine e;
	String color, modelName;
	float cost,milage;
	protected int noOfWheels, people_capacity;
	protected File f;
	
	Vehicles(String mn)						//Creates a file with model name as file name
	{
		e=new Engine();
		color="Silver";
		modelName=mn;
		f=new File(modelName +".dat");
	}

	String getModelName()						//returns model name for comparision
	{
		return modelName;
	}

	void editModel()						//Edit model properties
	{
		int c;
		String st;
		float ft;
		Scanner sc= new Scanner(System.in);
		System.out.println("What to add? 1-color 2-model 3-cost 4-milage 5-type of fuel 6-number of cylinders");
		c=sc.nextInt();
		System.out.println("Enter value/model");
		
		switch(c)
		{
		case 1:
			st=sc.next(); color=st; break;
		case 2:
			st=sc.next(); modelName=st; f=new File(modelName+".dat");break;
		case 3:
			ft=sc.nextFloat();cost=ft; break;
		case 4:
			ft=sc.nextFloat();milage=ft; break;
		case 5:
			st=sc.next(); e.setFuel(st); break;
		case 6:
			ft=sc.nextFloat();e.setCylinders((int)ft); break;
		default:
			System.out.println("Enter a valid choice");
		}

	}
	
	void writeToFile() throws Exception				//function to write details to file
	{
		FileWriter fw= new FileWriter(f);
		fw.write("Color: "+color);
		fw.write("\nModel: "+modelName);
		fw.write("\ncost: "+cost);
		fw.write("\nMilage: "+milage);
		fw.write("\nNumber of people: "+people_capacity);
		fw.write("\nNo of wheels: "+ noOfWheels);
		fw.write("\nType Of fuel: "+ e.typeOfFuel);
		fw.write("\nNumber Of cylinders: "+ e.noOfCylinders);
		fw.write("\n");
		fw.close();
	}	
	void readFromFile()throws Exception				//reading from a file
	{
		FileReader fr=new FileReader(f);
		int ch;

		while((ch=fr.read()) != -1)
		{
			System.out.print((char)ch);
		}
		fr.close();
	}	
	
	abstract void editFeature();
	abstract void write() throws Exception;
}


class TwoWheeler extends Vehicles					//Two wheeler data
{
	TwoWheeler(String mn)
	{
		super(mn);
		noOfWheels=2;	
		people_capacity=2;
	}
		
	void editFeature()
	{
		System.out.println("No additional Features can be added");
	}
	
	void write() throws Exception					//writes details of Vehicles
	{
		writeToFile();
	}	

}

abstract class FourWheeler extends Vehicles				//Four wheelers
{
	String radio,ac;

	FourWheeler(String mn)
	{
		super(mn);
		noOfWheels=4;
		people_capacity=5;
	}
	
	abstract void write() throws Exception;	
	abstract void editFeature();
}

class PrivateCar extends FourWheeler					//Private car data
{
	String tv,speakers, film;

	PrivateCar(String mn)
	{
		super(mn);
		tv="none";
		speakers="none";
		film="none";
	}
		
	void editFeature()						//Adding/Editing features
	{
		int ch;
		String st;
		Scanner sc= new Scanner(System.in);
		System.out.println("What to add? 1-TV 2-Speakers 3-Film");
		ch=sc.nextInt();
		System.out.println("Enter model name and number:");
		st=sc.next();
		
		switch(ch)
		{
		case 1:
			tv=st; break;
		case 2:
			speakers=st; break;
		case 3:
			film=st; break;
		default:
			System.out.println("Enter a valid choice");
		}

	}

	void write() throws Exception					//Writing to a file
	{
	
		FileWriter fw= new FileWriter(f,true);
		fw.write("\nT.V. Type:"+ tv);
		fw.write("\nSpeakers Type:"+ speakers);
		fw.write("\nFilm Type:"+ film);
		fw.write("\n");
		writeToFile();
		fw.close();
	}

}

class CommercialCar extends FourWheeler            //data for commercial car
{
	String stand;
	
	CommercialCar(String mn)
	{
		super(mn);
		stand="none";
	}

	void write() throws Exception					//Writing to a file
	{
		FileWriter fw= new FileWriter(f,true);
		fw.write("\nStand Type:"+ stand);
		writeToFile();
		fw.close();
	}
	
	void editFeature()						//Adding/Editing features
	{
		String s;
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter model name and number of the stand:");
		s=sc.next();
		stand=s;
	}
}

class Transport								//Contains MENU and object creation
{
	public static void main(String args[])			
	{
		int choice,pos=0;	
		int ch,i=0;
		String s;
		Scanner sc= new Scanner(System.in);
									//Number of maximum vehicles entered
		System.out.println("Enter how many vehicles you would like to store:");
		choice=sc.nextInt();
		
		Vehicles v[]=new Vehicles[choice];

		for(choice=0;choice!=4;)				//MENU
		{
			System.out.println("MENU \n\n");
			System.out.println("1-Add a new vehicle\n");
			System.out.println("2-Edit Details\n");
			System.out.println("3-Read details\n");
			System.out.println("4-EXIT\n");
			choice=sc.nextInt();
			
			switch(choice)
			{
			case 1:						//Adding new objects using dynamic polymorphism
				System.out.println("Which vehicle to add\n");
				System.out.println("1-Two wheeler\n");
				System.out.println("2-Private Car\n");
				System.out.println("3-Commercial Car\n");
				ch=sc.nextInt();

				System.out.println("\n Enter Model:");
				s=sc.next();
				if(ch==1)
					v[pos++]=new TwoWheeler(s);
				else if(ch==2)
					v[pos++]=new PrivateCar(s);
				else if(ch==3)
					v[pos++]=new CommercialCar(s);
				else
					System.out.println("\n\n Error");
				try{v[pos-1].write();}    //check for exceptions and writing to file
				catch(Exception e)
				{System.out.println("\n\n Error:"+e);}
				 break;
			case 2:						//Editing details of vehicles
				System.out.println("\n Enter model name");
				s=sc.next();
				for(i=0;i<pos && !s.equals(v[i].getModelName());i++){}
				System.out.println("\n 1-Edit model details \n2-Edit features");
				ch=sc.nextInt();
				if(ch==1)
					v[i].editModel();
				else if (ch==2)
					v[i].editFeature();
				try{v[i].write();}        //check for exceptions and writing to file
				catch(Exception e)
				{System.out.println("\n\n Error:"+e);}
				 break;
			case 3:						//Reading details from vehicles
				System.out.println("\n Enter model name");
				s=sc.next();
				for(i=0;i<pos && !s.equals(v[i].getModelName());i++){}
				try{v[i].readFromFile();}    //check for exceptions and reading a  file
				catch(Exception e)
				{System.out.println("\n\n Error:"+e);}
				break;	
			case 4:                    //exit
				break;		
			default:
				System.out.println("\nEnter a valid choice");
			}
		}
	}
}