		JAVA							|					C++
									|
1.Does not support pointers, templates, unions, structures, etc.	|	1.supports pointers, templates, unions, structures, etc.
2.Java supports automatic garbage collections.				|	2.You have to define a destructor for garbaging at end of execution of the	
									|	program
3.Does not support default arguments. No scope resolution operator (::)	|	3.Scope resolution operator is present. Hence methods can be declared outside
  hence all methods have to be defined in the class.			|	of classes as well.
4.Supports only method overloading and not operation overloading.	| 	4.Both method as well as operator overloading are supported.
5.Built in support for documentation of comments (/**.....*/)		|	5.Does not support documentation of comments.
6.Static quoted strings are directly converted into String objects	|	6.Strings exist as independent static character arrays
7.Arrays have different structures and behaviors. There is a read-only 	|
  member that tells you how big the array is and run-time exception 	|
  checking that throws exception when you go out of bounds		|
8. All objects of non-primitive type are created via new		|	8.You can create non-primitive objects "on the stack"
9. There are wrapper classes for all primitive classes			|
10.No preprocessors- like macros. Hence all the packages have to be 	|	10.Preprocessors exist. Hence libraries have to be included.
   imported								|
11.Packets collect library components under a single library name	|	11.Namespace is used to prevent ambiguity.
12. Singly rooted hierarchy is present. Hence the concept of multiple 	|	12.New inheritance can be defined anywhere. Virtual is used to avoid  		   inheritance is absent						|	ambiguities found during multiple inheritance				|
13.Exception handlers are superior. Exceptions are checked and enforced	|	13.Function is called at run time and if something goes wrong the exception 
   at compile time. 							|	is thrown.
14. Access Specifiers (private, public and protected) are placed in 	|	14.Are in form of controlling blocks, i.e. explicit specifiers
   each definition for each member of a class. Hence element defaults 	|
   to "friendly", which means that it is accessible by all others in 	|
   the class.