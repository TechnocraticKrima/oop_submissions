/* Design a class called "Point" in java that can store a point of n-dimensions
 decided dynamically during creation of an object of the class. Create appropriate
  constructors(including copy constructors). Create a member function of the class
  that would compare two points. Create an array of objects of the point class
  and use the features of the class in main function.

                                                -Krima 16/3/15  */
import java.util.Scanner;

class Points                                        //class for storing n-dimensional array
{
    int i,dimension;
    int point[];

	Scanner s=new Scanner(System.in);

    Points(){}

    Points(int n)                                   //inputting the point
    {
        dimension=n;
        point=new int[n];
        for(i=0;i<n;i++)
        {
            System.out.println("Enter the element"+i);
            point[i]=s.nextInt();
        }
    }

    Points(Points p)                               //copy constructor
    {
        dimension=p.dimension;
        point=new int[dimension];
        for(i=0;i<dimension;i++)
        {
            point[i]=p.point[i];
        }

    }


	void PrintPoints()
	{
	    for(i=0;i<dimension;i++)
		{
		    System.out.println(i+" : "+point[i]);
		}
	}


	boolean ComparePoints(Points p2)                         //comparing two points
	{
		    int diff=0;
		    if (dimension!=p2.dimension)
		    {
			System.out.println("\n Dimensions do not match");
			return false;
		    }
		    for(int i=0;i<dimension;i++)
			{
				if(point[i]!=p2.point[i])
					diff++;
			}
		    if (diff==0)
			{return true;}
		    else
		    {
			System.out.println("\n Not equal");
			return false;
		    }
	}
}


class Point
{

	public static void main(String args[])
	{
	    int n, d,choice=0, i=0;

		Scanner s=new Scanner(System.in);

	    System.out.println("Enter number of points to be entered");
	    n=s.nextInt();

	    Points p[]=new Points[n];

	    while(choice!=4)
	    {
		System.out.println("MENU \n ******************************** \n");
		System.out.println(" 0-Display the point\n1-Add a point \n 2-Compare two points \n 3-Copy a point \n 4-EXIT");
		choice=s.nextInt();

		switch(choice)
		{
		case 0:
		        if ( i+1 != n )
		        {
		            int j ;
		            for ( j = 0 ; j < i ; j++ ) {
		                    p[j].PrintPoints();
		    
		            }

		        }
		        break ;
		case 1:
		    if (i+1==n)
		    {
		        System.out.println("You have reached your limit");
		        break;
		    }
		    System.out.println("Enter the dimensions of the point:");
		    d=s.nextInt();
		    p[i++]=new Points(d);
		    break;

		case 2:
		    System.out.println("Enter the point 1 to be compared:");
		    d=s.nextInt();
		    System.out.println("Enter the dimensions of the point:");
		    choice=s.nextInt();
		    if(p[d].ComparePoints(p[choice]))
		        System.out.println("Points are equal");
		    choice=0;
		    break;

		case 3:
		    if (i+1==n)
		    {
		        System.out.println("You have reached your limit");
		        break;
		    }
		    System.out.println("Enter the point to be copied:");
		    d=s.nextInt();
		    p[i++]=new Points(p[d]);
		    break;

		case 4:
		    break;

		default:
		    System.out.println("Please enter a valid choice");
		}

	    }
	}

}
