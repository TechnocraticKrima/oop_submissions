/* Design a class in C++ for implementing the queue data structure of dynamic length (decided
during object creation).
-Use appropriate constructors and destructor for ensuring optimal memory
utilization.
-Overload the binary “+” operator to perform EnQueue operation, and “– –” operator to
perform DeQueue operation. The Dequeue operation should return the element removed from the
queue. Also, overload the “<<” operator to display all the elements of the queue. Use namespace(s)
to avoid any possible name clash.
                                            -Krima 12/3/15  */

#include <iostream>

using namespace std;

class Queue                                                     //class for storing the elements of queue
{
    friend std::ostream& operator <<(std::ostream&, Queue&);    // operator overloading for cout
private:
    int num,i;
    int *q;
    int position=0;
public:
    Queue(){}                                                           //default constructor

    Queue(int n)                                                        //parameterized constructor
    {
        num=n;
        q=new int[n];
    }

    ~Queue()                                                            //destructor
    {
        delete q;
    }
    void operator +(int);                                               // operator overloading for entering elements
    int operator --();                                                  // operator overloading for obtaining elements

};

void Queue::operator+(int x)                                            // operator overloading for entering elements
{
    if (position+1>num)
    {
        cout<<"Queue is full";
        return;
    }
    q[position]=x;
            cout<<"\n"<<q[position];
    position++;
}

int Queue::operator --()                                                // operator overloading for obtaining elements
{
    int first;
    if (position==0)
        return -999;
    first=q[0];
    for (i=1;i!=position;i++)
    {
        q[i-1]=q[i];
    }
    position--;
    return first;
}

std::ostream& operator<<(std::ostream &out, Queue &q1)                  // operator overloading for cout
{
    int pos;
    for(pos=0;pos<q1.position;pos++)
    {
        out<<"\n Element #"<<pos<<"is ";
        out<<q1.q[pos];
    }
}

int main()                                                              //contains menu for queue
{
    int num;
    char choice='\0';
    cout<<"Enter the limit of elements you would like to enter upto:";
    cin>>num;

    Queue q1(num);

    while (choice!='x'||'X')
    {
        cout<<"\nWhat would you like to do? \n\t\t***************************\n";
        cout<<"E-enqueue an element \n D-Dequeue a element \n S-show all queued elements\n X-exit";
        cin>>choice;

        switch(choice)
        {
        case 'e':case 'E':                                              //enqueues an element
            cout<<"\n Enter the number to be queued:";
            cin>>num;
            q1+num;
            break;

        case 'd':case 'D':                                              //dequeues an element
            cout<<"\n Number dequeued:"<<--q1;
            break;

        case 's':case 'S':                                              //shows all elements
            cout<<"\n All queued elements:"<<q1;
            break;

        case 'x':case 'X':                                              //exit
            break;

        default:
            cout<<"\n Enter a valid choice.";
        }
    }

}
