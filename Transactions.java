/*	Create an abstract class called “Account” and inherit it into two classes called “Savings” and
“Current” in Java. Create appropriate data members with suitable visibility modes for each of the
classes. Also create appropriate constructors and member functions for these classes such that they
are able to implement the following business rules:
- An account created should either be a savings account or a current account.
- A savings account can only be opened with a minimum balance of `1,000/-
- Savings accounts need to maintain a minimum balance of `500/- at any point of time.
- Interest is calculated and credited in savings accounts only once in each quarter.
- The current accounts can have different overdraft limits ranging from `500/- to `1 Cr.
- No current account would be allowed to have a negative balance beyond the overdraft limit
Implement the above mentioned classes through a menu driven program that would allow the user
to select the type of account at the time of account creation as well as making any type of
transaction. Use run-time polymorphism to implement the classes.

Modify to store the account details in binary
files and allow the user to create as well as delete an account. The user should also be allowed
to perform operations on existing accounts through a menu.		-Krima 19/3/15 */

import java.util.*;
import java.io.*;


class OverLimitWithdrawalException extends Exception				//for throwing error of withdrawing over limit that is defined
{
	public String toString(){	
		return "Your limit exceeds predefined balance.";
	}
}

class InvalidDepositException extends Exception					//for throwing error of depositing a negative value
{
	public String toString(){	
		return "Deposit amount cannot be negative";
	}
}


abstract class Account								//Stores basic details of any account 
{
	protected double balance;
	private String accountNo;
	protected int nxt_month,nxt_day, nxt_year;
	private int c_month,c_day, c_year;
	File f;

	Account(String s, double d)						//constructor
	{
		accountNo=s;
		balance=d;
		Calendar today=Calendar.getInstance();
		c_day=today.get(Calendar.DATE);
		c_month=today.get(Calendar.MONTH);
		c_year=today.get(Calendar.YEAR);
		
		f= new File(accountNo+".dat");	

		nxt_day=today.get(Calendar.DATE);
		nxt_month=today.get(Calendar.MONTH);
		nxt_year=today.get(Calendar.YEAR);

	}

	protected void writeToFile(File f)					//Writing to file for Savings
	{
		try{
			FileWriter fw=new FileWriter(f);
			fw.write("Account Details\n \n");
			fw.write("Account Number:"+ accountNo +"\n");
			fw.write("Balance:"+ balance+"\n");
			fw.write("Creation Date:" + c_day + "/" + c_month + "/" + c_year);
			fw.close();
		}catch(IOException e)
		{
			System.out.println(e);
		}

	}

	protected void writeToFile(File f, double od)				//Writing to file for Current
	{
		try{
			FileWriter fw=new FileWriter(f);
			fw.write("Account Details\n \n");
			fw.write("Account Number:"+ accountNo +"\n");
			fw.write("Balance:"+ balance+"\n");
			fw.write("Creation Date:" + c_day + "/" + c_month + "/" + c_year);
			fw.write("Overdraft Limit:" + od);
			fw.close();
		}catch(NullPointerException e)
		{System.out.println(e);}
		catch(IOException e)
		{System.out.println(e);}

	}


	void deposit(double m)	throws Exception					//depositing money to a account
	{
		if (m>=0)
		{
			balance+=m;
			writeToFile(f);
			System.out.println("Deposit Successful!");
		}
		throw new InvalidDepositException();
	}
	
	void viewDetails()							//viewing a account using FileReader
	{
		try{
			FileReader fr=new FileReader(f);
			int ch;
			while((ch=fr.read())!=-1)
				System.out.println((char)ch);
		}catch(NullPointerException e)
		{System.out.println(e);}
		catch(IOException e)
		{System.out.println(e);}
	}


	String getAccountNo()
	{
		return accountNo;
	}
	
	abstract void withdraw(double out)throws Exception;
	abstract void change(double od);
}



class Savings extends Account							//Savings account details					
{
	private float interestRate=10;
	private double minBal=500;
	
	Savings(String acc, double bal)						//constructor
	{
		super(acc,bal);
		writeToFile(f);	
			
	}
	

	void set_interest(float x)						//setting interest rate(by bank)
	{
		if (x>=0 && x<=100)
			interestRate=x;
		else
			System.out.println("Interest not reset. Interest rate should lie between 0 and 100");	
	}
		
	void change(double od)							//changing balance as per interest
	{
		int t_day,t_month,t_year;
		Calendar today=Calendar.getInstance();
		t_day=today.get(Calendar.DATE);
		t_month=today.get(Calendar.MONTH);
		t_year=today.get(Calendar.YEAR);
		if(t_year>=nxt_year)
		{
			if(t_month>=nxt_month)
			{	
				if (t_day>=nxt_day)
				{
					if (nxt_month+3>=12)
					{
						nxt_year+=1;
						nxt_month=12-nxt_month;
					}
					else
						nxt_month+=3;
					balance+=(interestRate*balance/100);
					System.out.println("Updation a Success! Your balance:"+balance);
					writeToFile(f);

				}
			}
		}
	}

	void withdraw(double out)throws Exception						//withdrawing money keeping a minimum balance 500
	{
		if (balance-out>=minBal)
		{
			balance-=out;
			writeToFile(f);
			System.out.println("Withdrawal Successful! Your balance:"+balance);
		}
		else
			throw new OverLimitWithdrawalException();
	}

}


class Current extends Account							//Current account details
{
	private double overdraftLimit;

	Current(String acc, double bal, double od)				//constructor
	{
		
		super( acc, bal );
		overdraftLimit=od;
		writeToFile(f,od);		
	
	}

	void withdraw(double out)throws Exception					//withdrawing upto -(overdraft limit)
	{
		if(balance-out>=(-overdraftLimit))
		{
			balance-=out;
			System.out.println("Withdrawal Successful! Your balance:"+balance);
			writeToFile(f);
		}
		else
			throw new OverLimitWithdrawalException();
	}
	
	void change(double od)							//changing overdraft limit
	{
		if(od<=1000000 && od>500)
		{
			overdraftLimit=od;
			writeToFile(f,od);
		}
		else
			System.out.println("Overdraft Limit should lay between 500 and 1000000");
	}
}

class Transactions								//performing transactions on accounts
{
	
	public static void main(String args[])
	{
		int i=0,k=0,ch=0,choice=0;
		String AccCheck;
		double open=0,od=0;
		Scanner sc= new Scanner(System.in);
		Account a[]=new Account[100];

	while(choice != 3)							//menu to select Savings/Current account
	{	System.out.println("MAIN MENU\n ************************\n");
		System.out.println("\n Which account would you like to view?\n");
		System.out.println("\n\t1-Savings Account");
		System.out.println("\n\t2-Current Account");
		System.out.println("\n\t3-EXIT");
		choice=sc.nextInt();
		
		switch(choice)
		{
		case 1:case 2:							//Common MENU
			ch=0;							//reset ch to 0
			while(ch!=6)
			{
				System.out.println("MENU\n ************************\n");
				System.out.println("\n\t1- Make a new account");
				System.out.println("\n\t2- View your account");
				System.out.println("\n\t3- Deposit");
				System.out.println("\n\t4- Withdraw");
				System.out.println("\n\t5- Calculate PMI/Change Overdraft Limit");
				System.out.println("\n\t6- EXIT");
				ch=sc.nextInt();
	
				switch(ch)
				{
				case 1:						//checks for distinct account number
					System.out.println("Enter your account number");
					AccCheck=sc.next();
					for(k=0;k<i && !AccCheck.equals(a[k].getAccountNo()); k++){}	//a[k].getAccountNo() != AccCheck only checks value
					if (k < i)
						System.out.println("Account already exists");
					else if(choice==1)			//minimum balance for savings account
					{
						System.out.println("Enter the initial deposit amount");
						open=sc.nextFloat();
						if(open>=1000)
							a[i++]=new Savings(AccCheck,open);
						else	
							System.out.println("Account creation unsuccessful. \n Minimum balance should be 1000");
					}
					else					//overdraft limit for current account
					{
						System.out.println("Enter the initial deposit amount");
						open=sc.nextFloat();
						System.out.println("Enter the overdraft limit");
						od=sc.nextFloat();
						if(od<=1000000 && od>500)
							a[i++]=new Current(AccCheck,open,od);
						else	
							System.out.println("Account creation unsuccessful. \n Overdraft Limit should lay between 500 and 1000000");
					}
					break;
				case 2:						//view any account
					System.out.println("Enter your account number");
					AccCheck=sc.next();
					for(k=0; k<i &&  !AccCheck.equals(a[k].getAccountNo()); k++){}
					if (AccCheck.equals(a[k].getAccountNo()))
						a[k].viewDetails();
					break;
				case 3:						//deposit into any account
					System.out.println("Enter your account number");
					AccCheck=sc.next();
					System.out.println("Enter amount to be deposited:");
					od=sc.nextFloat();
					for(k=0; k<i && !AccCheck.equals(a[k].getAccountNo()); k++){}
					try{
					if (AccCheck.equals(a[k].getAccountNo()))
						a[k].deposit(od);
					}
					catch(Exception e)
					{System.out.println("Error that occurred:"+e);}
					break;
				case 4:						//withdraw from any account
					System.out.println("Enter your account number");
					AccCheck=sc.next();
					System.out.println("Enter amount to be withdrawn:");
					od=sc.nextFloat();
					for(k=0; k<i && !AccCheck.equals(a[k].getAccountNo()); k++){}
					try{
					if (AccCheck.equals(a[k].getAccountNo()))
						a[k].withdraw(od);
					}
					catch(Exception e)
					{
						System.out.println("Error that occurred:"+e);	
					}	
					break;
				case 5:						//Calculate PMI for Savings or Change Overdraft Limit for Current
					System.out.println("Enter your account number");
					AccCheck=sc.next();
					for(k=0; k<i && !AccCheck.equals(a[k].getAccountNo()); k++){}
					if (AccCheck.equals(a[k].getAccountNo()))
					{
						if (choice==1)
						{
							a[k].change(od);
						}
						else
						{
							System.out.println("Enter overdraft limit:");
							od=sc.nextFloat();
							a[k].change(od);
						}	
					}
					break;
				case 6:						//EXIT
					break;
				default:
					System.out.println("Please enter a valid choice");
		
				}


				}
			break;
			case 3:							//EXIT from menu
				break;
			default:
				System.out.println("Enter a valid choice");
			}
		}
	}
}