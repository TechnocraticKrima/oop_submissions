/* Creating a dynamic array for performing the following operations on the list:

(a) Add an item in the list

(b) Delete an item from the list

(c) Check whether the list contains duplicate elements (show with frequency)

(d) Remove duplicate values of elements from the list

(e) Sort the list in ascending order

(f) Sort the list in descending order

(g) Reverse the list

(h) Calculate maximum, minimum, sum, mean, median, mode, and standard deviation		
															-Krima 12/2/15	*/


import java.util.Scanner;
import static java.lang.Math.sqrt;

class List
{
	static int i,k=0;							//initializers required by every function
	static int dup_index[];							//index of duplicate numbers
	
	public static void main(String arg[])
	{
		int n,choice;
		int dataList[];							//array of numbers						
	
		Scanner s=new Scanner(System.in);
		System.out.println("Enter the number of numbers you would like to enter:");
		n=s.nextInt();
		
		dataList=new int[n];						//creating a dynamic array of n elements

		for(i=0;i<n;i++)
		{
			System.out.println("Enter number" + i + ":  ");
			dataList[i]=s.nextInt();

		}
		for (choice=0; choice!=9;)					//choice menu
		{
			System.out.println("MENU \n\t \t What would you like to do?");
			System.out.print(" \n\t 1-add a number \n\t 2-delete a number \n\t 3-Check for duplicates \n\t 4-delete duplicate numbers \n\t 5-sort ascending \n\t 6-sort descending \n\t 7-reverse \n\t 8-calcuate\n\t 9-exit");
			choice=s.nextInt();
			
			switch(choice)
			{	
			case 1:							//add element to array
				dataList=add(dataList,n);
				n++;						//increments the number of elements
				print(dataList,n);				//prints result after each function is performed
				break;
	
			case 2:							//delete element of array
				dataList=delete(dataList);
				print(dataList,n);
				break;
			case 3:							//Checks for duplicates in array
				checkDuplicate(dataList,n);
				print(dataList,n);
				break;	
			case 4:							//Delete duplicates in array to 0
				dataList=deleteDuplicate(dataList,n);
				print(dataList,n);
				break;
			case 5:							//sort ascending
				dataList=sortAsc(dataList,n);
				print(dataList,n);
				break;
			case 6:							//sort descending
				dataList=sortDes(dataList,n);
				print(dataList,n);
				break;
			case 7:							//reverse the array
				dataList=reverse(dataList,n);
				print(dataList,n);
				break;
			case 8:							//perform calculations on the array
				calculate(dataList,n);
				break;
			case 9:							//exit from the array
				break;
			default:
				System.out.println("Please enter a valid choice");
				break;
			}


		}
	}

	
	static int[] add(int list[], int num)				//function to add a element
	{
		int temp[]=new int[num+1];
		System.arraycopy(list,0,temp,0,num);
		Scanner s=new Scanner(System.in);
		System.out.println("Enter number" + (num+1) + ":  ");
		temp[num]=s.nextInt();
		return temp;
		 

	}

	static int[] delete(int arr[])					//function to delete a element
	{
		System.out.println("Enter the number of the element you would like to delete:");
		arr[i]=0;	
		
		return arr;

	}
				
	static void checkDuplicate(int arr[],int n)			//function to check for duplicate values
	{
		int j;
		dup_index=new int[n];
		for (i=0;i<n;i++)
		{
			for (j=0;j<i;j++)
			{
				if (arr[j]==arr[i])
				{	
					dup_index[k]=j;
					k++;
				}	
		
			}
		}
		print(dup_index,k);
	}

	static int[] deleteDuplicate(int arr[],int n)			//function to delete duplicate values
	{
		for (i=0;i<=k;k++)
		{
			arr[dup_index[i]]=0;
		}
		return arr;

	}
	
	static int[] reverse(int arr[],int num)				//function to reverse the array
	{
		for (i=0;i<num/2;i++)
		{
			int temp;
			temp=arr[i];
			arr[i]=arr[num-i-1];
			arr[num-i-1]=temp;
		}
		return arr;
	}

	static void calculate(int arr[],int n)				//function to perform calculations in array
	{
		int j,max,min,sum=0, median,mODE=0;
		int mode[][]=new int[n][2];
		double std_dev=0,mean;

		for (i=0;i<n;i++)					// a loop
		{
			mode[k][0]=arr[i];				//value of the number
			mODE=0;						//setting mode of number to 0
			for (j=0;j<i;j++)
			{	
				if (arr[j]>arr[i])			//for ascending sort
				{	
					int temp;
					temp=arr[i];
					arr[i]=arr[j];
					arr[j]=temp;
				}	
			}
			
			for(j=0;j<n;j++)
			{
				if (arr[j]==arr[i])			//checking mode
				{	
					mode[k][1]=++mODE;
				}
			}
			sum+=arr[i];					//taking sum
			k++;
		}
		
		for(i=0;i<k-1;i++)					//checking for number having maximum mode
		{
			if(mode[i][1]<mode[i+1][1])
				mODE=mode[i+1][0];				
		}

		max=arr[n-1];						//taking max,min and median of sorted array
		min=arr[0];
		median=arr[n/2];

		mean=sum/n;

		for(i=0;i<n;i++)					//calculating standard deviation
		{
			std_dev+=(arr[i]-mean)*(arr[i]-mean);
		}
		std_dev/=n;		
		std_dev=sqrt(std_dev);
		
		System.out.println( "\n Maximum is :" +max);		//printing output
		System.out.println( "\n Minimum is :" +min);
		System.out.println( "\n Median is :" +median);
		System.out.println( "\n Mean is :" +mean);
		System.out.println( "\n Mode is :" +mODE);
		System.out.println( "\n Sum is :" +sum);
		System.out.println( "\n Standard Deviation is :" +std_dev);
		


	}

	static int[] sortAsc(int arr[], int num)			//function to sort array in ascending order
	{
		int j;
		for (i=0;i<num;i++)
		{
			for (j=0;j<i;j++)
			{
				if (arr[j]>arr[i])
				{	
					int temp;
					temp=arr[i];
					arr[i]=arr[j];
					arr[j]=temp;
				}	
		
			}
		}
		return arr;

	}

	static int[] sortDes(int arr[], int num)			//function to sort array in descending order
	{
		int j;	
		for (i=0;i<num;i++)
		{
			for (j=0;j<i;j++)
			{
				if (arr[j]<arr[i])
				{	
					int temp;
					temp=arr[i];
					arr[i]=arr[j];
					arr[j]=temp;
				}	
		
			}
		}
		return arr;

	}

	static void print(int arr[], int num)				//print all elements of array
	{
		for(i=0;i<num;i++)	
		{
			System.out.println( i + ": " + arr[i] + " \n");
		}
	

	}
}

