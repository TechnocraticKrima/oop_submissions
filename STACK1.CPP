/*Design a class for implementing the stack data structure of dynamic length (decided during
object creation). Use appropriate constructors and destructor for ensuring optimal memory
utilization.			*/

#include <iostream>

using namespace std;

class LetterStack
{
public:
	char *letters;
	int number;
	int counter;

	LetterStack()
	{
		letters=NULL;
		number=0;
		counter=0;
	}

	LetterStack(int n)
	{
		number=n;
		cout<<n;
		letters=new char[number];
		counter=0;
	}

	~LetterStack()
	{
		delete letters;
	}
	char pop();
	int push(char);
};

char LetterStack::pop()
{
	if (counter>0)
	{
	    cout<<counter;
	    return letters[--counter];
	}
	else
		return '\0';


}

int LetterStack::push(char name)
{
	if (counter<number)
	{
		letters[counter++]=name;
		return 1;
	}
	else
		return 0;

}

int main()
{
	int number,choice;
	char name;
	cout<<"enter number of characters to be entered:";
	cin>>number;
	LetterStack s(number);
	while(choice!=0)
	{
		cout<<"\n    MENU    ";
		 cout<<"\n 1. Push";
		cout<<"\n 2. Pop";
		cout<<"\n 0. Exit";
		cout<<"\n Enter your choice: ";
		cin>>choice;

		switch(choice)
		{
		    case 1:  					//Push
			cout<<"\n\nEnter a letter: ";
			cin>>name;
			number=s.push(name);
			if(number==0)
			    cout<<"\n\nStack is Full!";
			break;
		    case 2:  					//Pop
			name = s.pop();
			if(name == '\0')
			    cout<<"\n\n The stack is Empty";
			else
			    cout<<"Popped Value: "<<name;
			break;
		    case 0:  					//Exit
			break;
		    default:					//default
			cout<<"Please enter a valid choice";
			break;
	}
	}
   return 0;
}

