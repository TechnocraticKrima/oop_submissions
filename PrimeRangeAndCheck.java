/* A program of Prime numbers where:
		1.You can check if a number is prime or not
		2.You can print numbers of a particular range			
							-Krima  11/02/15	*/

import java.util.Scanner;

class PrimeRangeAndCheck
{

	public static void main(String args[])
	{
		int choice;
		
		Scanner s=new Scanner(System.in);

		System.out.println("MENU \n\t \t What would you like to do?");
		System.out.print(" \n\t 1-Check if a number is prime or not. \n\t 2-Print prime numbers for a range.");
		choice=s.nextInt();
		
		switch(choice)
		{	
		case 1:
			int num;
			System.out.print(" Enter the number you would like to check:");
			num=s.nextInt();
		
			Prime(num);
			break;

		case 2:
			int num1,num2;
			System.out.print(" Enter the starting number:");
			num1=s.nextInt();
			System.out.print(" Enter the ending number:");
			num2=s.nextInt();

			Prime(num1,num2);
			break;
		default:
			System.out.print("Please enter a valid number");
			break;
		}

	}



	static void Prime(int num)
	{
		int i,count=0;
		for(i=2;i<=num/2;i++)
		{
			if (num%i == 0)
				count++;		
		}
		
		if (count==0 && num!=1)
			System.out.print("Number is prime");
		else
			System.out.print("Number is not prime");
	}

	static void Prime(int num1,int num2)
	{
		int i,j,count;
		System.out.print("Prime Numbers: ");
		for(i=num1;i<num2;i++)
		{
			for(j=2,count=0;j<=i/2;j++)
			{
				if (i%j == 0)
					count++;		
			}
			if (count==0 && i!=1)
				System.out.print( i + "\t" );		
		}
	}


}

