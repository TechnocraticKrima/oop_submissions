/* Design the following classes in Java by selecting appropriate data members, constructors and
member functions:
Other data members and/or member functions should also be included apart from those
mentioned in the above diagram. Restrict the creation of objects for “Shape”, “2D”, and “3D”
classes. Create appropriate member functions for checking the validity of values stored in
various classes and generate exceptions in case of invalid values. Create a menu driven program
in order to allow the user to create different shapes and perform various operations. Also, allow
the user to store and retrieve shapes from different binary files. Use exception handling
mechanism to ensure valid operations on files.
							-Krima 9/4/15		*/

import java.util.*;
import java.io.*;

class verticesException extends Exception			//For invalid vertices input
{
	public String toString(){
	return "Invalid input of vertices";}
}

class DistanceNegativeException extends Exception		//For a negative value of distance entered
{
	public String toString(){
	return "Value of Distance cannot be negative";}
}

abstract class Shape						//storing the color of the shapes
{
	protected final float PI=3.14F;
	private String color;

	Shape()
	{
		color="white";
	}

	public String get_color()				//getting colour
	{
		return color;
	}

	public void set_color(String c)				//set a colour
	{
		color=c;
	}
	
	abstract public void findAreaVol()throws Exception;
	abstract public void findSurfaceArea()throws Exception;
}

abstract class TwoD extends Shape				//Calculate the Area of 2D shape
{
	abstract public void findAreaVol()throws Exception;

	public void findSurfaceArea()throws Exception
	{
		System.out.println("Finding Surface Area of 2D objects is invalid");
	}

}

abstract class ThreeD extends Shape				//Calculate the Surface area and volume of 3D shape
{
	abstract public void findSurfaceArea()throws Exception;
	abstract public void findAreaVol()throws Exception;

}


class Rectangle extends TwoD
{
	int vertices[][];
	Rectangle()						//Constructor setting to 0
	{
		vertices=new int[2][5];
		for(int i=0;i<2;i++)
		{	
			vertices[i][0]=i+1;
			for(int j=1;j<5;j++)
			{
				vertices[i][j]=0;
			}
		}
	}
		
	public void findAreaVol()throws Exception		//To find Area of a Rectangle after checking validity of vertices
	{
		Scanner sc=new Scanner(System.in);
		int length,width;
		for(int i=0;i<2;i++)				//initializing vertices
		{	
			vertices[i][0]=i+1;
			for(int j=1;j<5;j++)
			{
				System.out.println("Enter vertice:"+i+" "+j+":");
				vertices[i][j]=sc.nextInt();
			}
		}
		
		for(int i=1;i<5;i++)				//sorting ascending as per X coordinate
		{
			for(int j=2;j<i;j++)
			{
				if(vertices[0][i]<vertices[0][j])
				{
					int temp1=vertices[0][i];
					int temp2=vertices[1][i];
					vertices[0][i]=vertices[0][j];
					vertices[1][i]=vertices[1][j];
					vertices[0][j]=temp1;
					vertices[1][j]=temp2;
				}
		
			}	
		}
		
		for(int i=1;i<5;i++)				//sorting 2 adjacent vertices ascending as per Y coordinate
		{

			if(vertices[1][i]<vertices[1][i-1] && (i==2 || i==4))
			{
					int temp1=vertices[0][i];
					int temp2=vertices[1][i];
					vertices[0][i]=vertices[0][i-1];
					vertices[1][i]=vertices[1][i-1];
					vertices[0][i-1]=temp1;
					vertices[1][i-1]=temp2;
			}	
		}

		length=vertices[1][2]-vertices[1][1];
		width=vertices[0][3]-vertices[0][2];
		if(length!=vertices[1][4]-vertices[1][3] || width!=vertices[0][4]-vertices[0][1])//checking validity
			throw new verticesException();
		else
		{		
			float a;
			a=length*width;
			System.out.println("Area is "+ a);
		}
	}
}

class Circle extends TwoD
{
	private float radius;
	private int center[];
		
	Circle()								//Constructor setting to 0
	{
		center= new int[3];
		center[0]=0;
		center[1]=0;
		center[2]=0;
		radius=0;
	}
	
	public void findAreaVol()throws Exception				//Find Area of a circle
	{
		Scanner sc=new Scanner(System.in);
		
		for(int j=0;j<3;j++)
		{
			System.out.println("Enter center "+j+":");
			center[j]=sc.nextInt();
		}
		
		System.out.println("Enter radius:");
		radius=sc.nextFloat();
		
		if(radius<0)
			throw new DistanceNegativeException();			//Throws exception for radius<0
		else
		{		
			float a;
			a=radius*radius*PI;
			System.out.println("Area is "+ a);
		}
	}
}
/*
class Cuboid extends ThreeD
{
	private int vertices[][];
	
	Cuboid()								//Constructor setting to 0
	{
		vertices[][]=new vertices[4][5];
		for(int i=0;i<4;i++)
		{	
			vertices[i][0]=i+1;
			for(int j=1;j<5;j++)
			{
				vertices[i][j]=0;
			}
		}

	}
	
	public void findAreaVol()throws Exception
	{
		Scanner sc=new Scanner(System.in);
		int length,width;
		for(int i=0;i<2;i++)
		{	
			vertices[0][i]=i+1;
			for(int j=1;j<5;j++)
			{
				System.out.println("Enter vertice:"+i+" "+j+":");
				vertices[j][i]=sc.nextInt();
			}
		}
		
		for(int i=1;i<4;i++)
		{
			for(j=2;j<i;j++)
			{
				if(vertices[0][i]<vertices[0][j])
				{
					temp1=vertices[0][i];
					temp2=vertices[1][i];
					vertices[0][i]=vertices[0][j];
					vertices[1][i]=vertices[1][j];
					vertices[0][j]=temp1;
					vertices[1][j]=temp2;
				}
				if(vertices[1][i]<vertices[1][j])
				{
					temp1=vertices[0][i];
					temp2=vertices[1][i];
					vertices[0][i]=vertices[0][j];
					vertices[1][i]=vertices[1][j];
					vertices[0][j]=temp1;
					vertices[1][j]=temp2;
				}
			}
		}
		length=vertices[1][1]-vertices[1][2];
		width=vertices[0][3]-vertices[0][2];
		if(length!=vertices[1][3]-vertices[1][4]||width!=vertices[0][4]-vertices[0][1])
			throw new verticesException();
		else
		{		
			float a;
			a=length*width;
			System.out.println("Volume is "+ a);
		}
	}


} 
*/
class Sphere extends ThreeD
{
	private float radius;
	private int center[];

	Sphere()								//Constructor setting to 0
	{
		center=new int[3];
		center[0]=0;
		center[1]=0;
		center[2]=0;
		radius=0;
	}	
	
	public void findAreaVol()throws Exception				//To find Surface Area of a Sphere
	{
		Scanner sc=new Scanner(System.in);
		
		for(int j=0;j<3;j++)
		{
			System.out.println("Enter center "+j+":");
			center[j]=sc.nextInt();
		}
		
		System.out.println("Enter radius");
		radius=sc.nextFloat();
		
		if(radius<0)
			throw new DistanceNegativeException();			//Throws exception for radius<0
		else
		{		
			float v;
			v=4*radius*radius*radius*PI/3;
			System.out.println("Volume is "+ v);
		}
	}
	
	public void findSurfaceArea()throws Exception				//To find Surface Area of a Sphere
	{
		Scanner sc=new Scanner(System.in);
		
		for(int j=0;j<3;j++)
		{
			System.out.println("Enter center "+j+":");
			center[j]=sc.nextInt();
		}
		
		System.out.println("Enter radius:");
		radius=sc.nextFloat();
		
		if(radius<0)
			throw new DistanceNegativeException();			//Throws exception for radius<0
		else
		{		
			float a;
			a=4*radius*radius*PI;
			System.out.println("Surface area is "+ a);
		}

	}
}


class ShapeOp
{
	public static void main(String args[])
	{
		int num,shapeCount=0, choice=0;
		Scanner sc=new Scanner(System.in);
		System.out.println("How many objects would you like to create?");		//Creating a number of objects
		num=sc.nextInt();
		Shape s[]=new Shape[num];

		while(choice != 5)								//MENU
		{
			System.out.println("MENU \n What would to like to do?");
			System.out.println("\n\t1-Make a new shape \n\t2-Calculate Area/Volume \n\t3-Surface Area(for 3D objects only) \n\t4-Set a Different Colour\n\t5-exit");
			choice= sc.nextInt();
			switch(choice)
			{
				case 1:								//Creating a paricular choice
					int ch=0;
					
					System.out.println("What would you like to make?");
					System.out.println("\n\t1-Rectangle \n\t2-Circle \n\t3-Sphere \n\t4-Cuboid");
					ch=sc.nextInt();
				
					switch(ch)
					{	
					case 1:
						s[shapeCount++]=new Rectangle();break;		
					case 2:
						s[shapeCount++]=new Circle();break;
					case 3:
						s[shapeCount++]=new Sphere();break;
					case 4:
						//s[shapeCount++]=new Cuboid();break;
					default:
						System.out.println("Enter a valid choice");									
					}
					break;
				case 2:								//For finding the Area/Volume of all Objects
					try{int i=0;
					System.out.println("Enter index of shape to find Area/Volume of:");
					i=sc.nextInt();
					s[i].findAreaVol();
					}catch(Exception e)
					{System.out.println(e);}
					break;

				case 3:								//For finding the Surface Area Of 3D objects
					try{int j=0;
					System.out.println("Enter index of shape to find Surface Area of:");
					j=sc.nextInt();
					s[j].findSurfaceArea();
					}catch(Exception e)
					{System.out.println(e);}
					break;
				case 4: 							//set colour
					int c=0;
					String co;
					System.out.println("Enter index of shape to change colour:");
					c=sc.nextInt();
					System.out.println("Enter colour:");
					co=sc.next();
					s[c].set_color(co);
					break;
				case 5:break;							//EXIT
				default:							//For other choices
					System.out.println("Enter a valid choice");
			}	

		}
	}
}